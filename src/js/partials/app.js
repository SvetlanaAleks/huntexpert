const App = (function () {
  "use strict";
  const reviewsSlider = $("#js_reviews-expert");
  return {
    initSlider: function (selector) {
      // slider init
      $(selector).slick({
        infinite: true,
        dots: false,
        speed: 500,
        prevArrow: '<div class="arr arr--left"><i class="fico fico-arr-left"></i></div>',
        nextArrow: '<div class="arr arr--right"><i class="fico fico-arr-right"></i></div>',
        fade: true,
        cssEase: "linear",
        autoplay: false,
        responsive: [{
            breakpoint: 992,
            settings: {
              adaptiveHeight: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              adaptiveHeight: false
            }
          },
        ]
      });
    },
    initSliderReview: function (selector) {
      // slider init
      $(selector).slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<div class="arr arr--left"><i class="fico fico-arr-left"></i></div>',
        nextArrow: '<div class="arr arr--right"><i class="fico fico-arr-right"></i></div>',
        responsive: [{
            breakpoint: 1200,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              adaptiveHeight: true
            }
          }
        ]
      });
    },
    showCommentExepert: function () {
      $(document).on("click", ".js_show-more", function () {
        const _this = $(this);
        const comment = _this.siblings(".info");
        comment.toggleClass("info--full");
      });
      reviewsSlider.on("beforeChange", function (
        event,
        slick,
        currentSlide,
        nextSlide
      ) {
        $(".info").removeClass("info--full");
      });
    },
    init: function () {
      this.initSlider(".js_slider-expert");
      this.initSliderReview(".js_slider-reviews");
      this.showCommentExepert();
    }
  };
})();